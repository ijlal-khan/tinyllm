from transformers import AutoModelForQuestionAnswering, AutoTokenizer

# Load model & tokenizer
model = AutoModelForQuestionAnswering.from_pretrained("saved_model")
tokenizer = AutoTokenizer.from_pretrained("saved_model")

# Get predictions using pipeline
nlp = pipeline('question-answering', model=model, tokenizer=tokenizer)
QA_input = {
    'question': 'Tell about me',
    'context': 'i am a student and doing computer sceinces and work on IT related things'
}
res = nlp(QA_input)

print("Answer:", res['answer'])
print("Score:", res['score'])
