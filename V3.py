import numpy as np
import tensorflow as tf

# Define the model's vocabulary
vocabulary = list("!$&',-.3:;?ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")

# Create stoi (String to Integer) and itos (Integer to String) mappings
stoi = {ch: i for i, ch in enumerate(vocabulary)}
itos = {i: ch for i, ch in enumerate(vocabulary)}

# Load the TensorFlow Lite model and allocate tensors
interpreter = tf.lite.Interpreter(model_path="models/model.tflite")
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Initialize the starting context with a valid token
start_token = 'A'  # Choose a start token that is within your vocabulary
context = np.array([[stoi[start_token]]], dtype=np.int64)

# Initialize an empty list to store the generated tokens
generated_tokens = []

# Set the temperature for diversity. Adjust as necessary.
temperature = 0.8

# Text generation loop
for _ in range(50):  # Generate 50 tokens; adjust as needed
    # Set the model's input tensor
    interpreter.set_tensor(input_details[0]['index'], context)

    # Run inference
    interpreter.invoke()

    # Extract the output tensor
    output_data = interpreter.get_tensor(output_details[0]['index'])
    output_data = np.array(output_data)

    # Apply temperature scaling to logits
    scaled_logits = output_data[0, 0, :] / temperature

    # Convert logits to a probability distribution
    probabilities = np.exp(scaled_logits) / np.sum(np.exp(scaled_logits))

    # Sample the next token index from the probability distribution
    predicted_index = np.random.choice(a=len(probabilities), p=probabilities)

    # Decode the predicted token index to a character
    decoded_token = itos[predicted_index]

    # Append the decoded token to the list of generated tokens
    generated_tokens.append(decoded_token)

    # Update the context with the predicted token index for the next iteration
    context = np.array([[predicted_index]], dtype=np.int64)

# Join the generated tokens to form the final generated text
generated_text = ''.join(generated_tokens)

# Print the generated text
print(generated_text)
