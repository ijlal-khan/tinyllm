import numpy as np
import tensorflow as tf

# Define the path to the TFLite model file
model_path = 'models/model.tflite'  # Make sure to replace this with the actual path to your model

# Load the TFLite model and allocate tensors
interpreter = tf.lite.Interpreter(model_path=model_path)
chars = ['\n',
 ' ',
 '!',
 '$',
 '&',
 "'",
 ',',
 '-',
 '.',
 '3',
 ':',
 ';',
 '?',
 'A',
 'B',
 'C',
 'D',
 'E',
 'F',
 'G',
 'H',
 'I',
 'J',
 'K',
 'L',
 'M',
 'N',
 'O',
 'P',
 'Q',
 'R',
 'S',
 'T',
 'U',
 'V',
 'W',
 'X',
 'Y',
 'Z',
 'a',
 'b',
 'c',
 'd',
 'e',
 'f',
 'g',
 'h',
 'i',
 'j',
 'k',
 'l',
 'm',
 'n',
 'o',
 'p',
 'q',
 'r',
 's',
 't',
 'u',
 'v',
 'w',
 'x',
 'y',
 'z']

# Create a starting context with a single token (using the first character in your vocabulary)
start_token = chars[0]  # Use the first character in your vocabulary
context = np.array([[stoi[start_token]]], dtype=np.int64)  # Change dtype to np.int64

# Initialize an empty list to store the generated tokens
generated_tokens = []

# ...

temperature = 0.8  # Adjust the temperature value

for _ in range(50):  # Adjust the number of tokens to generate as needed
    # Set the value of the input tensor with the context
    interpreter.set_tensor(input_details[0]['index'], context)

    # Run the inference
    interpreter.invoke()

    # Extract the output tensor and convert it to a NumPy array
    output_data = interpreter.get_tensor(output_details[0]['index'])
    output_data = np.array(output_data)

    # Apply the temperature to the logits before sampling
    scaled_logits = output_data[0, 0, :] / temperature

    # Use softmax to generate a probability distribution
    probabilities = np.exp(scaled_logits) / np.sum(np.exp(scaled_logits), axis=0)

    # Sample from the probability distribution
    predicted_index = np.random.choice(len(probabilities), p=probabilities)

    # Convert the index to an integer to get the predicted token
    predicted_token = int(predicted_index)

    # Decode the predicted token to a character
    decoded_token = itos.get(predicted_token, '<UNK>')

    # Append the decoded token to the generated text
    generated_tokens.append(decoded_token)

    # Update the context for the next token generation
    context = np.array([[predicted_token]], dtype=np.int64)

# ...


# Concatenate the generated tokens to form the final generated text
generated_text = ''.join(generated_tokens)

# Print the generated text
print(generated_text)
