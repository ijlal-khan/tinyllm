import torch
import torch.nn as nn
from torch.nn import functional as F

from SimplestGPT import BigramLanguageModel, decode,encode

# Model architecture definition here (same as the BigramLanguageModel class and its dependencies)

# Initialize the model
model_to_load = BigramLanguageModel()

# Load the model's state dictionary
PATH = 'models/model_state_dict.pt'
model_to_load.load_state_dict(torch.load(PATH))

# Ensure to call model.eval() to set dropout and batch normalization layers to evaluation mode before running inference.
model_to_load.eval()

# Device configuration
device = 'cuda' if torch.cuda.is_available() else 'cpu'
model_to_load.to(device)

# Now you can use model_to_load for predictions
# For example, to generate text:
prompt = 'I am happy because'
context = torch.tensor(encode(prompt), dtype=torch.long, device=device)
# generated_text = decode(model_to_load.generate(context, max_new_tokens=200).cpu().numpy()[0].tolist())
generated_chars = decode(model_to_load.generate(context.unsqueeze(0), max_new_tokens=100)[0].tolist())
print(generated_chars)


