import numpy as np
import tensorflow as tf

# Define your model's vocabulary
vocabulary = list("!$&',-.3:;?ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")

# Ensure the 'itos' and 'stoi' mappings are correctly defined
itos = {i: ch for i, ch in enumerate(vocabulary)}
stoi = {ch: i for i, ch in enumerate(vocabulary)}

# Load the TFLite model and allocate tensors
interpreter = tf.lite.Interpreter(model_path="models/model.tflite")
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Choose a start token that exists in your vocabulary
start_token = vocabulary[0]  # Adjust as needed, using the first character as an example
context = np.array([[stoi[start_token]]], dtype=np.int64)

# Initialize an empty list to store generated tokens
generated_tokens = []

# Set the temperature
temperature = 0.8

# Generation loop
for _ in range(50):  # Adjust the range if you want more or fewer characters
    # Set the context for the model
    interpreter.set_tensor(input_details[0]['index'], context)

    # Run inference
    interpreter.invoke()

    # Extract output
    output_data = interpreter.get_tensor(output_details[0]['index'])
    output_data = np.array(output_data)

    # Scale logits by temperature
    scaled_logits = output_data[0, 0, :] / temperature

    # Convert logits to probabilities
    probabilities = np.exp(scaled_logits) / np.sum(np.exp(scaled_logits))

    # Sample the next token
    predicted_index = np.random.choice(len(probabilities), p=probabilities)

    # Decode the token to a character
    decoded_token = itos.get(predicted_index, '<UNK>')

    # Append to generated tokens
    generated_tokens.append(decoded_token)

    # Update context with the predicted token for the next iteration
    context = np.array([[predicted_index]], dtype=np.int64)

# Concatenate generated tokens to form the final text
generated_text = ''.join(generated_tokens)

# Print the generated text
print(generated_text)
